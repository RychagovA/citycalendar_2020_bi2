<?php session_start();

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Добавление нового события!</title>
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic|Playfair+Display:400,700&subset=latin,cyrillic">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css">
  <link rel="stylesheet" type="text/css" href="style_add.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
</head>
<body>
    <header>
    <nav class="container">
      <a class="logo" href="index.php"  title="Вернуться на главную страницу">
        <span>А</span>
        <span>Ф</span>
        <span>И</span>
        <span>Ш</span>
        <span>А</span>
        <span>Н</span>
        <span>Н</span>
      </a>
      <ul id="menu">    
        <li><a href="contacts.html">Контакты</a></li>
        <li><a href="info.html">О сайте</a></li>
      </ul>
    </nav>
  </header>






<section class="new_article">
  <div class="registration_form">   
    <h1>Ввод новой статьи</h1>
    
      <form method="post" action="save_events.php" enctype="multipart/form-data"> 

        <p><h4>Название</h4>
          <div class="input-form"><input type="text" name="Name" value="" class="form-item" autofocus required></div>
              
       </p>
            <h4>ID Категории</h4> <div class="input-form"><input type="text" name="category_id" value="" class="form-item"  placeholder="1-Концерты, 2-Театры, 3-Кино, 4-Выставки, 5-Цирк, 6-Музеи, 7-Развлечения" autofocus required></div>
       </p>
      <p>
      <h4>Дата События</h4>
          <div class="input-form"><input type="date" name="Date" value="" class="form-item" required></div>
          
      </p>
         <h4>Изображение</h4>   
          <div class="input-form"><input type="file" name="Img" value="" class="form-item" required></div>
          
      
        <p><h4> Описание</h4>
         <div class="input-form"> <textarea name="Description" class="form-item" maxlength="600" required >
      </textarea></div>
         </p>
              <h4>Место</h4> <div class="input-form"><textarea name="Location" class="form-item" maxlength="600" required >
      </textarea></div>
          </p>
          <div class="input-form"><input type="submit" value="Сохранить" name="btn" class="btn"></div>
      <p>
    </div>
</section>
    
<!--     
<div class= "footer">
  <footer>
  <div class="container">
    <div class="footer-col"><span>Афиша Нижнего Новгорода 2020</span></div>
    <div class="footer-col">
     
    </div>
   
  </div>
</footer>
</div>

 -->

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>



  </body>
</html>