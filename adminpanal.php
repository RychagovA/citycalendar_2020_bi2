<!doctype html>
<html lang="ru">
<head>
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
      
  <title>Админ-панель</title>
</head>
<body>
  <?php
    $host = 'localhost';  // Хост, у нас все локально
    $user = 'root';    // Имя созданного вами пользователя
    $pass = 'root'; // Установленный вами пароль пользователю
    $db_name = 'citycalendar';   // Имя базы данных
    $link = mysqli_connect($host, $user, $pass, $db_name); // Соединяемся с базой

    // Ругаемся, если соединение установить не удалось
    if (!$link) {
      echo 'Не могу соединиться с БД. Код ошибки: ' . mysqli_connect_errno() . ', ошибка: ' . mysqli_connect_error();
      exit;
    }

     //Если переменная Name передана
      if (isset($_POST["Name"])) {
    //Вставляем данные, подставляя их в запрос
    $sql = mysqli_query($link, "INSERT INTO `products` (`Name`, `text`, `Date`, `Img`, `Description`, `Location`) VALUES ('{$_POST['Name']}', '{$_POST['text']}', '{$_POST['Date']}', '{$_POST['Img']}', '{$_POST['Description']}', '{$_POST['Location']}')");
    //Если вставка прошла успешно
    if ($sql) {
      echo '<p>Данные успешно добавлены в таблицу.</p>';
    } else {
      echo '<p>Произошла ошибка: ' . mysqli_error($link) . '</p>';
    }
  ?>
  <div class="container">   
    <h1>Ввод новой статьи</h1>
    <div>
      <form method="post" action=""> <p><label> Название<input type="text" name="Name" value="" class="form-item" autofocus required>
      </label> </p>
            <label> ID Категории<input type="text" name="category_id" value="" class="form-item" autofocus required>
      </label> </p>
      <p>
      <label>
          Дата События
          <input type="date" name="Date" value="" class="form-item" required>
      </label></p>
            <label>
          Изображение
          <input type="file" name="Img" value="" class="form-item" required>
      </label></p>
        <p><label>
          Описание
          <textarea name="Description" class="form-item" required >
      </textarea></p>
               Место
          <textarea name="Location" class="form-item" required >
      </textarea></p>
      <p><input type="submit" value="Сохранить" class="btn">
    </label>
    </div>
    
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>