<?php
session_start();
error_reporting(0);

$dbSettings = require 'db.php';

$mysqli = mysqli_connect('localhost', $dbSettings['user'], $dbSettings['password'], $dbSettings['dbName']);
mysqli_set_charset($mysqli,'utf8');

if(mysqli_connect_errno()) {
    echo "Не удалось подключиться к MySQL: " . mysqli_connect_error();
}
$id = $_GET['id'];
$search = $_GET['search'];
if ($search) {
  $query = "SELECT * FROM event WHERE Name like '%{$search}%' OR Description like '%$search%' OR category_id = '{$id}' ORDER BY id DESC";
} else {$query = "SELECT * FROM event WHERE category_id = '{$id}' ORDER BY id DESC";}
if($_SESSION['user']['id']){
    $auth = mysqli_query($mysqli,"SELECT * FROM users WHERE id = '{$_SESSION['user']['id']}'");
    $auth = mysqli_fetch_object($auth);}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>АфишаНиНо</title>
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic|Playfair+Display:400,700&subset=latin,cyrillic">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css">
  <link rel="stylesheet" type="text/css" href="style.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>

</head>
<body>
	<header>
    <nav class="container">
      <a class="logo" href="index.php"  title="Вернуться на главную страницу">
        <span>А</span>
        <span>Ф</span>
        <span>И</span>
        <span>Ш</span>
        <span>А</span>
        <span>Н</span>
        <span>Н</span>
      </a>
      <div class="nav-toggle"><span></span></div>
      <form action="" method="get" id="searchform">
        <input name = 'search' type="text" placeholder="Искать на сайте...">
        <button type="submit"><i class="fa fa-search"></i></button>
      </form>
      <ul id="menu">
       
        <li><a href="contacts.html">Контакты</a></li>
        <li><a href="info.html">О нас </a></li>
      </ul>
    </nav>
  </header>
  <div class="container">
    <div class="posts-list">
      <?php
        // Сортируем статьи обратном порядке
        $articles = mysqli_query($mysqli, $query);
        ?>
        <?php while($article = mysqli_fetch_assoc($articles)):
        $img = mysqli_query($mysqli,"SELECT * FROM event WHERE id = '{$article->id}'");
       $img = mysqli_fetch_object($img);
       //print_r($article['Img']); exit();?> 

        <article id="post-1" class="post">
            <div class="post-image"><a href="">
            <img src="/<?= $article['Img']?>" width="200" alt=""></a></div>
            <div class="post-content">
            <div class="category"><a href=""><?php
            $category = mysqli_query($mysqli, "SELECT * FROM category WHERE id = '{$article['category_id']}' ");
            echo mysqli_fetch_assoc($category) ['name'] ?></a></div>
            <h2 class="post-title"><?= $article['Name']  ?></h2>
            <p><?= $article['Description']  ?></p>
            <div class="post-date"> <p> Дата события :<b> <?=$article['Date'] ?></b> </div></p>
            <div class="post-location"> <p> Место проведения:<b>    <?=$article['Location'] ?></b> </div>
        </article>
      <?php endwhile; ?>
    </div>
  <aside>
    <div class="widget">
  <div class="auth">

                        <?php
                        if(!$_SESSION['user']['id']):
                        if($_SESSION['status']['error']):
                            ?>
<center style="color: red;">Вы не авторизовались <?=$_SESSION['status']['error'] ?></center>
                        <?php
                            unset($_SESSION['status']['error']);
                            ?>
                        <?php endif; ?>
                        <?php
                        if( $_SESSION['status']['success']):
                            unset($_SESSION['status']['success']);
                            ?>
                           <center  style="color: black;">Вы авторизовались</center>
                        <?php endif; ?>
 <div class="button"> <input type="submit" name="auth" onclick="window.location='/auth.php'" value="Войти">
<input type="submit" name="reg" onclick="window.location='/auth.php'" value="Зарегистрироваться"></li>
                      </div>
                        <?php else: ?>
                            <center style ="  font-size: 20px; margin-left: 30px;margin-right:30px; background: #fff;color: #F8B763;border-top-right-radius: 20px;border-bottom-right-radius: 20px;border-top-left-radius: 20px;border-bottom-left-radius: 20px; " > Вы авторизовались, <?= $auth->login ?></center><br>
                        <center>  <a href="logout.php" class="auth-button" style ="

                                margin-top: 20px;
                                color: #fff;
                                font-weight: bold;
                                text-transform: uppercase;
                                border: none;
                                background: black;
                                transition: .3s;">Выйти</a></center>
                        <?php endif; ?>

                </div>


</div>

  <div class="widget">
    <h3 class="widget-title">Категории</h3>
    <ul class="widget-category-list">
        <?php $categories = mysqli_query($mysqli, "SELECT * FROM category"); ?>
		<?php while($category = mysqli_fetch_assoc($categories)): ?>
        <li><a href="/category.php?id=<?= $category['id'] ?>"><?= $category['name'] ?></a> </li>
		<?php endwhile; ?>
    </ul>
  </div>
  <div class="widget">
  	<h3 class="widget-title">Календарь</h3>



<table id="calendar2">
  <thead>
    <tr><td>‹<td colspan="5"><td>›
    <tr><td>Пн<td>Вт<td>Ср<td>Чт<td>Пт<td>Сб<td>Вс
  <tbody>
</table>

<script>
function Calendar2(id, year, month) {
var Dlast = new Date(year,month+1,0).getDate(),
    D = new Date(year,month,Dlast),
    DNlast = new Date(D.getFullYear(),D.getMonth(),Dlast).getDay(),
    DNfirst = new Date(D.getFullYear(),D.getMonth(),1).getDay(),
    calendar = '<tr>',
    month=["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"];
if (DNfirst != 0) {
  for(var  i = 1; i < DNfirst; i++) calendar += '<td>';
}else{
  for(var  i = 0; i < 6; i++) calendar += '<td>';
}
for(var  i = 1; i <= Dlast; i++) {
  if (i == new Date().getDate() && D.getFullYear() == new Date().getFullYear() && D.getMonth() == new Date().getMonth()) {
    calendar += '<td class="today">' + i;
  }else{
    calendar += '<td>' + i;
  }
  if (new Date(D.getFullYear(),D.getMonth(),i).getDay() == 0) {
    calendar += '<tr>';
  }
}
for(var  i = DNlast; i < 7; i++) calendar += '<td>&nbsp;';
document.querySelector('#'+id+' tbody').innerHTML = calendar;
document.querySelector('#'+id+' thead td:nth-child(2)').innerHTML = month[D.getMonth()] +' '+ D.getFullYear();
document.querySelector('#'+id+' thead td:nth-child(2)').dataset.month = D.getMonth();
document.querySelector('#'+id+' thead td:nth-child(2)').dataset.year = D.getFullYear();
if (document.querySelectorAll('#'+id+' tbody tr').length < 6) {  // чтобы при перелистывании месяцев не "подпрыгивала" вся страница, добавляется ряд пустых клеток. Итог: всегда 6 строк для цифр
    document.querySelector('#'+id+' tbody').innerHTML += '<tr><td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;';
}
}
Calendar2("calendar2", new Date().getFullYear(), new Date().getMonth());
// переключатель минус месяц
document.querySelector('#calendar2 thead tr:nth-child(1) td:nth-child(1)').onclick = function() {
  Calendar2("calendar2", document.querySelector('#calendar2 thead td:nth-child(2)').dataset.year, parseFloat(document.querySelector('#calendar2 thead td:nth-child(2)').dataset.month)-1);
}
// переключатель плюс месяц
document.querySelector('#calendar2 thead tr:nth-child(1) td:nth-child(3)').onclick = function() {
  Calendar2("calendar2", document.querySelector('#calendar2 thead td:nth-child(2)').dataset.year, parseFloat(document.querySelector('#calendar2 thead td:nth-child(2)').dataset.month)+1);
}
</script>

</div>
<div class="widget">
  <a href="message_view.php">Прочитать сообщение</a>
</div>

</aside>
</div> <!-- конец div class="container"-->
  <footer>
  <div class="container">
    <div class="footer-col"><span>Афиша Нижнего Новгорода 2020</span></div>
    <div class="footer-col">

    </div>

  </div>
</footer>
<script >
$('.nav-toggle').on('click', function(){
$('#menu').toggleClass('active');
});
</script>
</body>
</html>